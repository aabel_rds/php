<?php

namespace App\Form;

use App\Entity\DemoRequest;
use PhpParser\Node\Stmt\Label;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DemoRequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('surname')
            ->add('email',EmailType::class)
            ->add('city',ChoiceType::class, [
                'choices' => [
                    'Madrid' =>'Madrid',
                    'Barcelona' =>'Barcelona',
                    'Valencia' =>'Valencia',
                ],
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DemoRequest::class,
        ]);
    }
}
