<?php

namespace App\Controller;

use App\Entity\Comments;
use App\Entity\DemoRequest;
use App\Form\CommentsType;
use App\Form\DemoRequestType;
use App\Repository\CommentsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MainController extends AbstractController{

    /**
     * @Route("/home", name="Maleteo")
     */

    // Creamos la ruta home,creamos el form, recogemos datos del form y lo seteamos en bbdd DemoRequest
    public function homePage(Request $request, EntityManagerInterface $doctrine){

        $form = $this->createForm(DemoRequestType::class);
        $form->handleRequest($request);
        $getComments = $doctrine->getRepository(Comments :: class);
        $commit='Solicitud Enviada';
        // obtenemos todos los comentarios de la bbdd
        $comments = $getComments->findAll();
        // De todos los comentarios nos quedamos con 3 de ellos aleatoriamente
        $random = array_rand($comments,3);
        $comment1 =$comments[$random[0]];
        $comment2= $comments[$random[1]];
        $comment3= $comments[$random[2]];
        // Guardamos dichos comentarios para mostrarlos en la web
        $randomized [] = $comment1;
        $randomized [] = $comment2;
        $randomized [] = $comment3;
        // \var_dump($randomized);
        
        // validamos el formulario
        if($form->isSubmitted() && $form->isValid() ){
            $this->addFlash('notice',$commit);
            $newRequest = $form->getData(); //recogemos los datos del formulario

            $doctrine->persist($newRequest); // tracked area
            $doctrine->flush();              // push to bbdd

            return $this->redirectToRoute('Maleteo');
        }


        return $this->render('maleteo.html.twig',['demoRequest' => $form->createView(), 'comments' => $randomized]);
    
    }




    /**
     * @Route("/Privacy", name="Política-privacidad")
     */
    public function privacyPolicy(){
        
        $political = '
        El presente Política de Privacidad establece los términos en que MALETEO, S.L usa y protege la información que es proporcionada por sus usuarios al momento de utilizar su sitio web. Esta compañía está comprometida con la seguridad de los datos de sus usuarios. Cuando le pedimos llenar los campos de información personal con la cual usted pueda ser identificado, lo hacemos asegurando que sólo se empleará de acuerdo con los términos de este documento. Sin embargo esta Política de Privacidad puede cambiar con el tiempo o ser actualizada por lo que le recomendamos y enfatizamos revisar continuamente esta página para asegurarse que está de acuerdo con dichos cambios.
        Información que es recogida:
        Nuestro sitio web podrá recoger información personal por ejemplo: Nombre,  información de contacto como  su dirección de correo electrónica e información demográfica. Así mismo cuando sea necesario podrá ser requerida información específica para procesar algún pedido o realizar una entrega o facturación.
        Uso de la información recogida:
        Nuestro sitio web emplea la información con el fin de proporcionar el mejor servicio posible, particularmente para mantener un registro de usuarios, de pedidos en caso que aplique, y mejorar nuestros productos y servicios.  Es posible que sean enviados correos electrónicos periódicamente a través de nuestro sitio con ofertas especiales, nuevos productos y otra información publicitaria que consideremos relevante para usted o que pueda brindarle algún beneficio, estos correos electrónicos serán enviados a la dirección que usted proporcione y podrán ser cancelados en cualquier momento.
        MALETEO, S.L está altamente comprometido para cumplir con el compromiso de mantener su información segura. Usamos los sistemas más avanzados y los actualizamos constantemente para asegurarnos que no exista ningún acceso no autorizado.
        Cookies:
        Una cookie se refiere a un fichero que es enviado con la finalidad de solicitar permiso para almacenarse en su ordenador, al aceptar dicho fichero se crea y la cookie sirve entonces para tener información respecto al tráfico web, y también facilita las futuras visitas a una web recurrente. Otra función que tienen las cookies es que con ellas las web pueden reconocerte individualmente y por tanto brindarte el mejor servicio personalizado de su web.
        Nuestro sitio web emplea las cookies para poder identificar las páginas que son visitadas y su frecuencia. Esta información es empleada únicamente para análisis estadístico y después la información se elimina de forma permanente. Usted puede eliminar las cookies en cualquier momento desde su ordenador. Sin embargo las cookies ayudan a proporcionar un mejor servicio de los sitios web, estás no dan acceso a información de su ordenador ni de usted, a menos de que usted así lo quiera y la proporcione directamente, . Usted puede aceptar o negar el uso de cookies, sin embargo la mayoría de navegadores aceptan cookies automáticamente pues sirve para tener un mejor servicio web. También usted puede cambiar la configuración de su ordenador para declinar las cookies. Si se declinan es posible que no pueda utilizar algunos de nuestros servicios.
        Enlaces a Terceros:
        Este sitio web pudiera contener en laces a otros sitios que pudieran ser de su interés. Una vez que usted de clic en estos enlaces y abandone nuestra página, ya no tenemos control sobre al sitio al que es redirigido y por lo tanto no somos responsables de los términos o privacidad ni de la protección de sus datos en esos otros sitios terceros. Dichos sitios están sujetos a sus propias políticas de privacidad por lo cual es recomendable que los consulte para confirmar que usted está de acuerdo con estas.
        Control de su información personal:        
        En cualquier momento usted puede restringir la recopilación o el uso de la información personal que es proporcionada a nuestro sitio web.  Cada vez que se le solicite rellenar un formulario, como el de alta de usuario, puede marcar o desmarcar la opción de recibir información por correo electrónico.  En caso de que haya marcado la opción de recibir nuestro boletín o publicidad usted puede cancelarla en cualquier momento.        
        Esta compañía no venderá, cederá ni distribuirá la información personal que es recopilada sin su consentimiento, salvo que sea requerido por un juez con un orden judicial.        
        MALETEO, S.L Se reserva el derecho de cambiar los términos de la presente Política de Privacidad en cualquier momento.';
        return $this->render('privacyPolicy.html.twig',['political'=>$political]);
    }

    /**
     * @Route("/Comments", name="Comentarios")
     */
    public function SetComments(Request $request, EntityManagerInterface $doctrine)
    {
        $form = $this->createForm(CommentsType::class);
        $form->handleRequest($request);
        $commit = 'Comentario enviado';
        if($form->isSubmitted() && $form->isValid() ){

            $this->addFlash('notice',$commit); // creamos un flash en la sesion para user feedback
            $newRequest = $form->getData();

            $doctrine->persist($newRequest);
            $doctrine->flush();

            return $this->redirectToRoute('Comentarios');
            
        }
        function editComment(Comments $comment, EntityManagerInterface $doctrine){
            $doctrine->remove($comment);
        }

        return $this->render('comments.html.twig',['comments' => $form->createView()]);
        
    }

    /**
     * @Route("/demoRequest",  name="Solicitudes")
     */
    public function showDemos(EntityManagerInterface $doctrine){
        $repository = $doctrine->getRepository(DemoRequest :: class);
        $requests = $repository->findAll();
        

        return $this->render('showDemoRequest.htm.twig',['requests' => $requests]);
    }

    /**
     * @Route("/showComments",  name="Opiniones")
     */
    public function showComments(EntityManagerInterface $doctrine){

        $repository = $doctrine->getRepository(Comments :: class);
        $comments = $repository->findAll();
        

        return $this->render('showComments.html.twig',['comments' => $comments]);
    }

}

