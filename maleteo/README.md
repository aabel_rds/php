# Instrucciones


### Ejecución del contenedor

Disponemos de un fichero _docker-compose.yml_ con el que levantar el contenedor y al mismo tiempo creará la imagen de Docker.

Antes de levantar el contenedor tenemos que modificar el fichero _docker-compose.yml_ y sustituir los valores de user y uid por los que correspondan.

En el fichero de ejemplo están kiko y 1000 

**nota:** también se utiliza el valor 1000 para decirle al servidor Apache que se ejcute con dicho id. 

El valor 1000 es id por defecto que se crea (en distribuciones Linux como por ejemplo Ubuntu), para el primer usuario, por lo tanto es posible que 
os sirva, lo único que tenéis que hacer es es cambiar el nombre de usuario por el de vuestro usuario en vuestra máquina.

Para saber el uid y el nombre de usuario ejecutar lo siguiente:

```
id
```  

dando como resultado algo parecido a esto:
 
 ```
uid=1000(kiko) gid=1000(kiko) groups=1000(kiko),4(adm).......
```

Una vez levantado, podemos asegurarnos que está todo correcto ejecutando:

```
docker-compose ps
```

Para 'entrar' en el contenedor utilizaremos la opción **-u** para indicar el usuario creado anteriormente:

```
docker-compose exec -u kiko php bash
```

Finalmente sólo nos queda instalar las dependencias del proyecto:

```shell script
composer install
```

Una vez hecho esto, en la URL http://localhost:8000 tendremos nuestra aplicación Symfony
recién instalada.


